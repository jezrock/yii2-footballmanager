<?php

use yii\db\Migration;

/**
 * Handles the creation of table `teams`.
 */
class m180105_202817_create_teams_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('teams', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'year' => $this->string(),
        ]);
        $this->createTable('players', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'surname' => $this->string(),
            'birthdata' => $this->string(),
            'field_position' => $this->string(),
            'team_id' => $this->string(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('teams');
        $this->dropTable('players');
    }
}

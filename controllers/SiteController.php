<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Players;
use app\models\Teams;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $delete_id = $request->post('delete_id');
        $model_team = Teams::find()->where(['id'=>"$delete_id"])->one();
        if($model_team) {
                $model_team->delete();
                $model_players = Players::find()->where(['team_id' => $delete_id]);
               if (!$model_players) {
                    $model_players->delete();
                }
        }

        $teams = Teams::find()->all();
        return $this->render('index.twig',array('teams'=>$teams,'delete_id'=>$delete_id));
    }

    public function actionTeam($team_id)
    {
        $access = Yii::$app->request->post("access");

        if($access)
        {
            $model = Players::find()->where(['id' => Yii::$app->request->post("delete_id")])->One();
            if($model) {
                $model->delete();
            }
        }
        $players = Players::find()->where('team_id' == $team_id)->all();
        return $this->render('team.twig', array('players' => $players,"team_id"=>$team_id));
    }

    public function actionTeamAdd()
    {
        $request = Yii::$app->request;
        $model = new Teams();

        $team['name'] = htmlspecialchars(trim($request->post('name')));
        $team['year'] = htmlspecialchars(trim($request->post('year')));

        $modelvalid = Teams::find()->where(['name' =>$team['name']])->One();

        $access = Yii::$app->request->post('addteam');
        if (!$modelvalid) {
            if (
            (isset($access) && isset($team['name']) && isset($team['year']))
            ) {
                $model->name = $team['name'];
                $model->year = $team['year'];
                $model->save();
                return $this->actionIndex();
            }
        }
        else return $this->render('team-add.twig',['team'=>$team,'massage'=>' This team was already added !!!']);

        return $this->render('team-add.twig');
    }

    public function actionTeamEdit($edit_id)
    {
        $request = Yii::$app->request;
        $model = Teams::find()->where(['id' =>$edit_id])->One();

        if ($model) {
            $team['name'] = $model->name;
            $team['year'] = $model->year;
        }

        $access = Yii::$app->request->post('access');

        if ($model) {
            if ($access) {

                $name = htmlspecialchars(trim($request->post('name')));

                $modelvalid = Teams::find()->where(['name' => $name])->One();

                if ((!$modelvalid) || ($model->name == $name)) {
                    $model->name = htmlspecialchars(trim($request->post('name')));
                    $model->year = htmlspecialchars(trim($request->post('year')));
                    $model->save();
                    return $this->actionIndex();
                } else return $this->render('team-edit.twig', ['team' => $team, 'massage' => ' This team is already in place !!!', 'edit_id' => $edit_id]);
            }
        }else return $this->render('team-edit.twig',['edit_id'=>$edit_id,'massage' => ' No such team !!!']);

        return $this->render('team-edit.twig',['team' => $team,"edit_id"=>$edit_id]);
    }

    public function actionPlayerAdd($team_id)
    {
        $request = Yii::$app->request;
        $access = $request->post('access');

        if($access)
        {
            $model = new Players();
            $model->name =  htmlspecialchars(trim($request->post('name')));
            $model->surname =  htmlspecialchars(trim($request->post('surname')));
            $model->birthdata =  htmlspecialchars(trim($request->post('birthdata')));
            $model->field_position =  htmlspecialchars(trim($request->post('field_position')));
            $model->team_id = (int)$team_id;
            $model->save();
            return $this->actionTeam($model->team_id);
        }
        return $this->render('player-add.twig',['team_id'=>$team_id]);
    }


    public function actionPlayerEdit($id)
    {
        $massage ="";
        $request = Yii::$app->request;
        $access = $request->post('access');

        $model = Players::find()->where(['id'=>$id])->One();

        if(($access)&&($model))
        {
            $model->name =  htmlspecialchars(trim($request->post('name')));
            $model->surname =  htmlspecialchars(trim($request->post('surname')));
            $model->birthdata =  htmlspecialchars(trim($request->post('birthdata')));
            $model->field_position =  htmlspecialchars(trim($request->post('field_position')));
            $model->save();
            return $this->actionTeam($model->team_id);
        }
        return $this->render('player-edit.twig',['player'=>$model,"massage"=>$massage]);
    }

}

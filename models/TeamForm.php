<?php

namespace app\models;

use Yii;
use yii\base\Model;

class TeamForm extends Model{

    public $name;
    public $year;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'year'], 'required' , "trim"],
            [['name'], 'min'>= 1 , 'max'<= 30],
            [['year'], 'min'>= 4 , 'max'<= 4],
        ];
    }

}
